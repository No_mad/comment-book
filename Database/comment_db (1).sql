-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Авг 06 2021 г., 15:19
-- Версия сервера: 5.7.35-0ubuntu0.18.04.1
-- Версия PHP: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `comment_db`
--
CREATE DATABASE IF NOT EXISTS `comment_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `comment_db`;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id_comments` int(11) NOT NULL,
  `text_comments` varchar(255) NOT NULL,
  `owner_comments` varchar(30) NOT NULL,
  `id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id_comments`, `text_comments`, `owner_comments`, `id_users`) VALUES
(6, 'This is my first comment!!!', 'jhon', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `subcomments`
--

CREATE TABLE `subcomments` (
  `id_subcomments` int(11) NOT NULL,
  `text_subcomments` varchar(255) NOT NULL,
  `owner_subcomments` varchar(30) NOT NULL,
  `id_users` int(11) NOT NULL,
  `id_comments` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `subcomments`
--

INSERT INTO `subcomments` (`id_subcomments`, `text_subcomments`, `owner_subcomments`, `id_users`, `id_comments`) VALUES
(13, 'My first subcomment!!!', 'david', 4, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `name_users` varchar(30) NOT NULL,
  `pass_users` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_users`, `name_users`, `pass_users`) VALUES
(3, 'jhon', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'),
(4, 'david', 'd5f12e53a182c062b6bf30c1445153faff12269a');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id_comments`),
  ADD KEY `id_users` (`id_users`);

--
-- Индексы таблицы `subcomments`
--
ALTER TABLE `subcomments`
  ADD PRIMARY KEY (`id_subcomments`),
  ADD UNIQUE KEY `owner_subcomments` (`owner_subcomments`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_comments` (`id_comments`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD UNIQUE KEY `name_users` (`name_users`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id_comments` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `subcomments`
--
ALTER TABLE `subcomments`
  MODIFY `id_subcomments` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`);

--
-- Ограничения внешнего ключа таблицы `subcomments`
--
ALTER TABLE `subcomments`
  ADD CONSTRAINT `subcomments_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`),
  ADD CONSTRAINT `subcomments_ibfk_2` FOREIGN KEY (`id_comments`) REFERENCES `comments` (`id_comments`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
