<?php

namespace CommentBook\Models;

class Subcomment extends Comment
{
    private int $parrent_id;

    public function __construct(string $text, string $owner, int $id = null, int $parrent_id = null)
    {
        if (!empty($parrent_id)) {
            $this->parrent_id = $parrent_id;
        }
        parent::__construct($text, $owner, $id);
    }

    public static function getAllSubcomments()
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare("SELECT * FROM `subcomments`");
        if ($stmt->execute([])) {
            return $stmt->fetchAll();
        } else {
            return [];
        }
    }

    public function publish(): bool
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare(
            "SELECT `id_users` FROM `users` WHERE BINARY
                                     `name_users` = :username"
        );
        if ($stmt->execute([
                               "username" => $this->owner
                           ])) {
            $data = $stmt->fetchAll();
        }
        if (!empty($data)) {
            $stmt = $pdo->prepare(
                "
            INSERT INTO `subcomments`
                (
                    `text_subcomments`,
                    `owner_subcomments`,
                    `id_comments`,
                    `id_users`
                )
            VALUES 
                (
                    :text,
                    :owner,
                    :id,
                    :id_users
                )"
            );
            $ret = $stmt->execute([
                                      "text" => $this->text,
                                      "owner" => $this->owner,
                                      "id" => $this->parrent_id,
                                      "id_users" => $data[0]["id_users"]
                                  ]);
            $this->id = $pdo->LastInsertId();
            return $ret;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getParrentId(): int
    {
        return $this->parrent_id;
    }

    /**
     * @param int $parrent_id
     */
    public function setParrentId(int $parrent_id): void
    {
        $this->parrent_id = $parrent_id;
    }

}