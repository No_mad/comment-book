<?php

namespace CommentBook\Models;


use PDO;

class DB
{
    private static $_instance;

    public function __construct()
    {
        self::$_instance = new PDO(
            "mysql:host=localhost;port=3306;dbname=" . $_ENV["DB_NAME"], $_ENV["DB_USER"],
            $_ENV["DB_PASS"],
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]
        );
    }

    public static function getInstance()
    {
        if (!self::$_instance) {
            new self();
        }
        return self::$_instance;
    }
}