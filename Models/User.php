<?php

namespace CommentBook\Models;

class User
{
    private string $username;
    private string $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }


    public function login(): ?User //return User or null
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare(
            "SELECT * FROM `users` WHERE BINARY
                                     `name_users` = :username AND 
                                     `pass_users` = :password"
        );
        if ($stmt->execute([
                               "username" => $this->username,
                               "password" => sha1($this->password)
                           ])) {
            $data = $stmt->fetchAll();
        }
        if (!empty($data)) {
            return $this;
        } else {
            return null;
        }
    }

    public function register(): bool
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare(
            "SELECT * FROM `users` WHERE BINARY
                                     `name_users` = :username"
        );
        if ($stmt->execute([
                               "username" => $this->username
                           ])) {
            $data = $stmt->fetchAll();
        }
        if (empty($data)) {
            $stmt = DB::getInstance()->prepare(
                "
        INSERT INTO `users` (
            `name_users`,
            `pass_users`
        )
        VALUES (
            :username,
            :password
        )
        "
            );
            return $stmt->execute([
                                      "username" => $this->username,
                                      "password" => sha1($this->password)
                                  ]);
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = sha1($password);
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }
}
