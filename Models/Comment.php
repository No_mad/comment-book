<?php

namespace CommentBook\Models;

class Comment
{
    protected int $id;
    protected string $text;
    protected string $owner;

    public function __construct(string $text, string $owner, int $id = null)
    {
        if (!empty($id)) {
            $this->id = $id;
        }
        $this->text = $text;
        $this->owner = $owner;
    }

    public function publish(): bool
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare(
            "SELECT `id_users` FROM `users` WHERE BINARY
                                     `name_users` = :username"
        );
        if ($stmt->execute([
                               "username" => $this->owner
                           ])) {
            $data = $stmt->fetchAll();
        }
        if (!empty($data)) {
            $stmt = $pdo->prepare(
                "
            INSERT INTO `comments`
                (
                    `text_comments`,
                    `id_users`,
                    `owner_comments`
                )
            VALUES 
                (
                    :text,
                    :id,
                    :owner
                )"
            );
            $ret = $stmt->execute([
                                      "text" => $this->text,
                                      "id" => $data[0]["id_users"],
                                      "owner" => $this->owner
                                  ]);
            $this->id = $pdo->LastInsertId();
            return $ret;
        }
        return false;
    }

    public static function getAllComments(): array
    {
        $pdo = DB::getInstance();
        $pdo->setAttribute($pdo::ATTR_DEFAULT_FETCH_MODE, $pdo::FETCH_ASSOC);
        $stmt = $pdo->prepare("SELECT * FROM `comments`");
        if ($stmt->execute()) {
            return $stmt->fetchAll();
        } else {
            return [];
        }
    }

    public static function redact(int $id, string $text): bool
    {
        $pdo = DB::getInstance();
        $stmt = $pdo->prepare("UPDATE `comments` SET `text_comments`=:text WHERE `id_comments`=:id");
        if ($stmt->execute([
                               "text" => $text,
                               "id" => $id
                           ])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     */
    public function setOwner(string $owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}