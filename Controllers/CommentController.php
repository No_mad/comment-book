<?php

namespace CommentBook\Controllers;

use CommentBook\Models\Comment;
use CommentBook\Models\Subcomment;
use CommentBook\Views\View;

class CommentController
{
    public function publish(array $request = null)
    {
        $error = "";
        if (!empty($request["text"])) {
            $new_comment = new Comment($request["text"], $_SESSION["current_username"]);
            if ($new_comment->publish()) {
                header("Location: /");
            } else {
                $error = 'error: create comment';
            }
        }
        $view = new View();
        $view->render("publish", ["error" => $error]);
    }

    public function index(array $request = null)
    {
        $data = Comment::getAllComments();
        $data1 = Subcomment::getAllSubcomments();

        $view = new View();
        $view->render("main", $data, $data1);
    }

    public function action(array $request = null) //redact comment or create subcomment
    {
        $error = "";
        if (!empty($request["comment_id"])) {
            $_SESSION["id_comment"] = $request["comment_id"];
        }

        if (isset($_POST["redact"]) && empty($_SESSION["check"])) {
            $_SESSION["check"] = "redact";
        }
        if (isset($_POST["create"]) && empty($_SESSION["check"])) {
            $_SESSION["check"] = "create";
        }

        if ($_SESSION["check"] == "redact") {//redact comment
            if (!empty($request["text"]) && !empty($_SESSION["id_comment"])) {
                if (Comment::redact($_SESSION["id_comment"], $request["text"])) {
                    unset($_SESSION["id_comment"]);
                    unset($_SESSION["check"]);
                    header("Location: /");
                } else {
                    $error = 'error: redact comment';
                }
            }
            $view = new View();
            $view->render("redact", ["error" => $error]);
        }
        if ($_SESSION["check"] == "create") {//publish subcomment
            if (!empty($request["text"]) && !empty($_SESSION["id_comment"])) {
                $new_subcomment = new Subcomment(
                    $request["text"],
                    $_SESSION["current_username"],
                    null,
                    $_SESSION["id_comment"]
                );
                if ($new_subcomment->publish()) {
                    unset($_SESSION["id_comment"]);
                    unset($_SESSION["check"]);
                    header("Location: /");
                } else {
                    $error = 'error: create subcomment';
                }
            }
            $view = new View();
            $view->render("publishSub", ["error" => $error]);
        }
    }
}