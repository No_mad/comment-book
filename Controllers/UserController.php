<?php

namespace CommentBook\Controllers;

use CommentBook\Models\User;
use CommentBook\Views\View;

class UserController
{

    public function login(array $request = null)
    {
        $error = "";
        if (empty($_SESSION["current_username"])) {
            if (!empty($request)) {
                $new_user = new User($request["name"], $request["password"]);
                if (!is_null($new_user->login())) {
                    //success
                    $_SESSION["current_username"] = $new_user->getUsername();
                    header("Location: /");
                } else {
                    //fail
                    $error = 'wrong username/password combination';
                }
            }
        } else {
            header("Location: /");
        }
        $view = new View();
        $view->render("login", ["error" => $error]);
    }

    public function register(array $request = null)
    {
        $error = "";
        if (!empty($request)) {
            $new_user = new User($request["name"], $request["password"]);
            if ($new_user->register()) {
                header("Location: /login");
            } else {
                $error = 'username is already taken';
            }
        }
        $view = new View();
        $view->render("register", ["error" => $error]);
    }

    public function logout(array $request = null)
    {
        unset($_SESSION["current_username"]);
        header("Location: /login");
    }
}