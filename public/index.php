<?php

use CommentBook\Models\DB;
use CommentBook\Routers\Router;

define("ROOT_PATH", dirname(__FILE__, 2));
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
$dotenv->load();
session_start();

$request = $_REQUEST ?? [];
if (!empty($_SESSION["current_username"])) {
    $path = $request["path"] ?? "/";
} else {
    $path = $request["path"] ?? "login";
}
unset($request["path"]);
$router = new Router();
$router->process($path, $request);
