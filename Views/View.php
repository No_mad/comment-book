<?php

namespace CommentBook\Views;

class View
{
    private string $ext = "php";
    private string $folder = "templates";


    public function setExt(string $ext)
    {
        $this->ext = $ext;
    }

    public function setFolder(string $folder)
    {
        $this->folder = $folder;
    }

    public function render(string $viewName, array $data = [], array $data1 = [])
    {
        require ROOT_PATH . DIRECTORY_SEPARATOR . $this->folder . DIRECTORY_SEPARATOR . $viewName . "." . $this->ext;
    }

}