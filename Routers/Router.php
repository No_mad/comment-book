<?php

namespace CommentBook\Routers;

class Router
{
    private array $routes = [];//info about routes

    public function __construct()
    {
        $this->Init();
    }

    public function Init()
    {
        $this->setRoute("/", "Comment", "index");
        $this->setRoute("login", "User", "login");
        $this->setRoute("register", "User", "register");
        $this->setRoute("publish", "Comment", "publish");
        $this->setRoute("action", "Comment", "action");
        $this->setRoute("logout", "User", "logout");
    }

    public function setRoute(string $url, string $controller, string $action): bool
    {
        if (empty($this->routes[$url])) { //if route don`t exist
            $this->routes[$url] = ["controller" => $controller, "action" => $action]; //create new route
            return true;
        } else {
            return false;
        }
    }

    public function getRoute(string $url): array
    {
        return $this->routes[$url] ?? []; //return route by url or empty array
    }

    public function process(string $url, array $request)
    {
        $route = $this->getRoute($url);
        if (empty($route)) {
        } else {
            $controllerName = "CommentBook\\Controllers\\" . $route["controller"] . "Controller";
            //(when $url='/')
            $controller = new $controllerName(); //$controller = new CommentController();
            $controller->{$route['action']}($request); //$controller->index($request);
        }
    }

}