<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Main Page </title>
    <style>
        Body {
            font-family: Calibri, Helvetica, sans-serif;
            background-color: pink;
        }

        button {
            background-color: #4CAF50;
            width: 100%;
            color: orange;
            padding: 15px;
            margin: 10px 0px;
            border: none;
            cursor: pointer;
        }


        form {
            border: 3px solid #f1f1f1;
        }

        input[type=text], input[type=password] {
            width: 100%;
            margin: 8px 0;
            padding: 12px 20px;
            display: inline-block;
            border: 2px solid green;
            box-sizing: border-box;
        }

        button:hover {
            opacity: 0.7;
        }

        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            margin: 10px 5px;
        }


        .container {
            padding: 25px;
            background-color: lightblue;
        }
    </style>
</head>
<body>
<center><h1> All comments </h1></center>

<form action="/logout">
    <?php
    echo "Welcome, " . $_SESSION["current_username"] . "<br>" ?>
    <button type="submit">Logout</button>
</form>
<form action="/publish">
    <button type="submit">Create new comment</button>
</form>


<?php
foreach ($data as $k => $v): ?>
    <form action="/action" method="POST">
        <div class="container">
            <div>
                <label>Owner : </label>
                <?php
                echo $v["owner_comments"] . "<br>"; ?>
            </div>
            <div>
                <label>Text : <br></label>
                <?php
                echo $v["text_comments"] . "<br><br>"; ?>
            </div>
            <div>
                <label>Subcomments : <br><br></label>
                <?php
                foreach ($data1 as $k1 => $v1): ?>
                    <?php
                    if ($v1["id_comments"] == $v["id_comments"]): ?>
                        <div>
                            <label>Owner : </label>
                            <?php
                            echo $v1["owner_subcomments"] . "<br>"; ?>
                        </div>
                        <div>
                            <label>Text : <br></label>
                            <?php
                            echo $v1["text_subcomments"] . "<br>"; ?>
                        </div>
                    <?php
                    endif; ?>
                <?php
                endforeach; ?>
            </div>
            <div>
                <input type="hidden" name="comment_id" value=<?php
                echo $v["id_comments"]; ?>>
                <?php
                if ($_SESSION["current_username"] === $v["owner_comments"]): ?>
                    <button type="submit" name="redact">Redact comment</button>
                <?php
                endif; ?>
                <?php
                if ($_SESSION["current_username"] != $v["owner_comments"]): ?>
                    <button type="submit" name="create">Create subcomment</button>
                <?php
                endif; ?>
            </div>
        </div>
    </form>
<?php
endforeach; ?>
</body>
</html>  